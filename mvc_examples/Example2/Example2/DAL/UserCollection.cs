﻿using Example2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example2.DAL
{
    public class UserCollection
    {
        public List<User> TheUsers;

        public UserCollection()
        {
            TheUsers = new List<User>
            {
                new User {FirstName = "Jim", LastName="Johnson", DOB = new DateTime(1940,6,2)}
            };
        }
    }
}