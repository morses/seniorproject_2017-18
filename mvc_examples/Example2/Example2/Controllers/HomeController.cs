﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // GET: ~/Home/Multiply
        /// <summary>
        /// GET a page with a form, that provides a user with 
        /// a way to multiply two numbers.  Uses the Viewbag to
        /// control whether or not the user sees the answer, rather
        /// than using the GET-POST-redirect to GET pattern of later 
        /// homeworks.
        /// </summary>
        /// <returns>A view with a form</returns>
        public ActionResult Multiply()
        {
            ViewBag.ShowAnswer = false;
            return View();
        }

        /// <summary>
        /// Process a POSTed form.  Expect at least the two numbers a and b.  Returns
        /// the answer in the ViewBag.
        /// </summary>
        /// <param name="name">The users name</param>
        /// <param name="a">The first operand</param>
        /// <param name="b">The second operand</param>
        /// <returns>a * b in a View</returns>
        [HttpPost]
        public ActionResult Multiply(string name, double? a, double? b)
        {
            
            if(a == null || b == null) // or check with a.HasValue 
            {
                ViewBag.ErrorMessage = "Please enter two numbers to multiply";
                ViewBag.ShowAnswer = false;
            }
            else
            {
                double answer = a.Value * b.Value;
                // or answer = (int)a * (int)b;
                ViewBag.ShowAnswer = true;
                ViewBag.Answer = answer;
                ViewBag.Name = name;
            }
            return View();
        }
    }
}