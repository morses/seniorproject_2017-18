﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Example1.Controllers
{
    public class HomeController : Controller
    {
        // Controller Action Methods
        // GET ~/Home/Index
        // GET ~/Home
        // GET ~/
        public ActionResult Index()
        {
            Debug.WriteLine("In Home Index()");
            return View();
        }

        // GET ~/Home/About
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            Debug.WriteLine("In Home About()");
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            Debug.WriteLine("In Home Contact()");
            return View();
        }

        public String WhatsMyName()
        {
            Debug.WriteLine("In Home WhatsMyName()");
            return "Scot";
        }

        public ActionResult QSExample1()
        {
            // Get parameters from the user (client) in query strings
            string theString = Request.QueryString["theString"];
            string age = Request.QueryString["age"];
            Debug.WriteLine($"{theString} and age is {age}");

            // Use it to build a custom page
            ViewBag.theString = theString;
            ViewBag.age = age;

            ViewData["theString2"] = theString;

            return View();
        }

        public ActionResult FormExample()
        {
            string name = Request.Form["name"];
            string password = Request.Form["password"];

            ViewBag.password = password;

            Debug.WriteLine($"{name} and  {password}");
            return View();
        }

        public ActionResult FormExample2(string name, string password, int? age)
        {
            //string name = Request.Form["name"];
            //string password = Request.Form["password"];

            ViewBag.password = password;
            ViewBag.age = age;

            Debug.WriteLine($"{name} and  {password}");
            return View();
        }

        // Example for Thursday 10/19/2017
        // Here's the pattern for part of a correct GET -> POST -> redirect to GET
        [HttpGet]
        public ActionResult FormExample3()
        {
            ViewBag.RequestMethod = "GET";
            return View();
        }

        [HttpPost]
        public ActionResult FormExample3(string name, string password, int? age)
        {
            ViewBag.RequestMethod = "POST";
            // What to do if we don't get something we need?  For now let's just
            // tell the client they're "Bad" :-)
            if(age == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Looks good, we can use this data
            Debug.WriteLine($"{name}:{password}:{age}");

            // And then redirect the user to another page (if desired)
            //return RedirectToAction("Index");

            // or return the same page
            return View();
        }
    }
}