﻿# Example for class of using Selenium IDE to test a web application
Feature: ID73 -- Search for classes
    As a student at WOU
    I would like to search for classes
    So that I can register for classes that meet my program requirements or
    So that I can look up class times, rooms and instructors

Scenario: Class search should be available
    Given I am visiting the "http://wou.edu" webpage
    When I search for "classes"
    Then the search results should contain "WOU: Real Time Class Availability" as a link
