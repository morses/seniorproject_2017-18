﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;

namespace IntegrationTestsWithSpecFlow.Tests
{
    [Binding]
    public class ID73_SearchForClassesSteps
    {
        IWebDriver driver = new FirefoxDriver();

        [Given(@"I am visiting the ""(.*)"" webpage")]
        public void GivenIAmVisitingTheWebpage(string p0)
        {
            driver.Navigate().GoToUrl(p0);
        }
        
        [When(@"I search for ""(.*)""")]
        public void WhenISearchFor(string p0)
        {
            IWebElement searchInputElement = driver.FindElement(By.Id("search-box-center"));
            searchInputElement.Click();
            searchInputElement.SendKeys(p0);
            searchInputElement.Submit();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(d => d.Title.StartsWith("Search results", StringComparison.OrdinalIgnoreCase));
            wait.Until(d => d.FindElement(By.CssSelector(".gsc-results")));
        }
        
        [Then(@"the search results should contain ""(.*)"" as a link")]
        public void ThenTheSearchResultsShouldContainAsALink(string p0)
        {
            IWebElement searchResultLink = driver.FindElement(By.LinkText(p0));
            Assert.IsNotNull(searchResultLink);
        }
    }
}
