# Example for class of using Selenium IDE to test a web application
Feature: ID73 -- Search for classes
    As a student at WOU
    I would like to search for classes
    So that I can register for classes that meet my program requirements or
    So that I can look up class times, rooms and instructors

Scenario: Class search should be available
    Given I am visiting the WOU.edu webpage
    When I search for classes
    Then the search results should contain a link to the class availability site "WOU: Real Time Class Availability"

Scenario: Class search should be the top result
    Given I am visiting the WOU.edu webpage
    When I search for classes
    Then the search results should have as the top result a link to the class availability site "WOU: Real Time Class Availability"

Scenario: Search for classes for a major
    Given I am visiting the class availability page at "http://www2.wou.edu/nora/registration.student.get_selection"
    And I am a Computer Science major
    When I search for classes for my major for next term
    Then I can see multiple classes that have CS as a dept prefix